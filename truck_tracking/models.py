from django.db import models


class TruckModel(models.Model):
    model_name = models.CharField(max_length=20)
    max_carrying = models.PositiveSmallIntegerField(default=100)

    def __str__(self):
        return self.model_name


class Truck(models.Model):
    board_number = models.CharField(max_length=10)
    truck_model = models.ForeignKey(TruckModel, on_delete=models.CASCADE)
    current_weight = models.PositiveSmallIntegerField(default=0)

    @property
    def overload(self):
        max_carrying = self.truck_model.max_carrying
        current_weight = self.current_weight

        if current_weight > max_carrying:
            return round((((current_weight - max_carrying) / max_carrying) * 100), 1)
        else:
            return None

    def __str__(self):
        return self.board_number
