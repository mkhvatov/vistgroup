from django.apps import AppConfig


class TruckTrackingConfig(AppConfig):
    name = 'truck_tracking'
